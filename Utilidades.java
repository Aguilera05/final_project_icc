import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;

/**
 * Clase que declara métodos para escribir y
 * leer en un archivo.
 * @author Adrián Aguilera Moreno.
 * <p> Creado el 10/02/2021 </p>
 * @version 1.1
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/io/package-summary.html">Java.io</a>
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Object.html">Object</a> 
 */

public class Utilidades{
    
    /* Atributos */
    private Object [] grupo = new Object[0];
    
    /**
     * <p>Método para agrupar artículos de cualquier clase.</p>
     * @param  objeto -- Elemento de la clase <code>Object</code>.
     * @return Object -- <code>grupo</code> de elementos genéricos.
     */
    public Object[] agrupar(Object objeto){
	Object[] temporal = new Object[grupo.length +1];
	for(int i=0; i< grupo.length; i++){
	    temporal[i] = grupo[i];
	}
	temporal[temporal.length -1] = objeto;
	grupo = temporal;
	return grupo;
    }
    
    /**
     * <p>Método que escribe objetos en un archivo de texto.</p>
     * @param ruta_archivo -- Ruta donde guardaremos los objetos.
     * @param grupo -- Arreglo que contiene los objetos que 
     *                 escribiremos.
     */
    public void escribirObjetos(String ruta_archivo, Object[] grupo){
	ObjectOutputStream escritor = null;
	try{
	    escritor = new ObjectOutputStream(new FileOutputStream(ruta_archivo));
	    for(int i=0; i< grupo.length; i++){
		escritor.writeObject(grupo[i]);
	    }
	} catch(IOException e){
	    System.out.println("Error al escribir...");
	    e.printStackTrace();
	} finally{
	    try{
		escritor.close();
	    } catch(IOException e){}
	}
    }
    
    /**
     * Método que hace la lectura de objetos grabados
     * en el archivo de texto.
     * @param ruta_archivo -- ruta del archivo donde
     * están grabados los objetos a leer.
     * @return Articulo -- Arreglo con objetos grabados
     * previamente en el archivo de texto.
     */
    public Object[] leerObjetos(String ruta_archivo){
	ObjectInputStream lector = null;
	try{
	    lector =
		new ObjectInputStream(new FileInputStream(ruta_archivo));
	    Object objeto;
	    do{//lector.available()
		objeto = lector.readObject();
		if(objeto != null){
		    agrupar(objeto);
		}
	    } while(objeto != null);
	} catch(java.lang.ClassNotFoundException e){
	    System.out.println("Error1.. ");
	    e.printStackTrace();
	} catch(java.io.EOFException e){
	    //System.out.println("Se terminó la lectura antes de lo esperado...");
	    //e.printStackTrace();
	} catch(IOException e){
	    System.out.println("Lectura fallida: ");
	    e.printStackTrace();
	} finally{
	    if(lector != null){
		try{
		    lector.close();
		} catch(IOException e){
		    e.printStackTrace();
		}
	    }
	}
	return grupo;
    }
}
