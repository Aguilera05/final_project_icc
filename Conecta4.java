/**
 * <p>Clase que construye un tablero para jugar <code>Conecta4</code>.</p>
 * @author Adrián Aguilera Moreno.
 * <p> Creado el 14/02/2021 </p>
 * @verion 1.0
 * @see <a href="./Documents/Jugador.html">Jugador</a>
 * @see <a href="./Documents/Juego.html">Juego</a>
 */

public class Conecta4 implements Juego{
    
    /* Atributos */
    private String tablero[][];
    
    /**
     * <p>Constructor por omisión. Crea
     * una cuadrícula 4x4 y llena de 
     * espacios en blanco las casillas.</p>
     */
    public Conecta4(){
	tablero = new String[4][4];
	for(int i=0; i < tablero.length; i++){
	    for(int j=0; j < tablero[i].length; j++){
		tablero[i][j] = " ";
	    }
	}
    }
    
    /**
     * Método que asigna <code>1</code> o <code>0</code> dependiendo
     * las posiciones vacias por columnas.
     * @param  fig         -- String: figura representante de la casilla.
     * @param  numColumna  -- int: Número de columna.
     * @param  secretaFila -- int: Número de fila.
     * @return boolean     -- <code>True</code> o <code>False</code>.
     */
    public boolean colocarTablero(String fig, int numColumna, int secretaFila){
	if(numColumna < 0 || numColumna > 3
	   || this.tablero[secretaFila][numColumna].equals("1")
	   ||  this.tablero[secretaFila][numColumna].equals("0")){
	    return false;	    
	} else{
	    tablero[secretaFila][numColumna] = fig;
	    return true;
	}
    }
    
    /**
     * <p>Método que regresa un boolean
     * indicando si terminó el juego
     * dependiendo del estado del
     * tablero.</p>
     * @return boolean -- <code>True</code> o
     *                    <code>False</code>.
     */
    @Override public boolean fin(){
	boolean temporal = false;
	int contador0 = 0;
	int contador1 = 0;
	if(!temporal){
	    // Verificación horizontal (Por fila).
	    for(int i=0; i < this.tablero.length; i++){
		for(int j=0; j < tablero[i].length; j++){
		    if(tablero[i][j].equals("0")){
			contador0++;
		    } else if(tablero[i][j].equals("1")){
			contador1++;
		    }
		}
		if(contador0 == 4 || contador1 == 4){
		    temporal = true;
		    break;
		}
		contador0 = 0;
		contador1 = 0;
	    }
	    contador0 = 0;
	    contador1 = 0;	    
	}
	
	if(!temporal){
	    // Verificación vértical (Por columna).
	    for(int i=0; i < tablero.length; i++){
		for(int j=0; j < tablero[i].length; j++){
		    if(tablero[j][i].equals("0")){
			contador0++;
		    } else if(tablero[j][i].equals("1")){
			contador1++;
		    }
		}
		if(contador0 == 4 || contador1 == 4){
		    temporal = true;
		    break;
		}
		contador0 =0;
		contador1 =0;
	    }
	    contador0 =0;
	    contador1 =0;
	}
	
	if(!temporal){
	    // Verificación contra la diagonal.
	    for(int i=0; i < tablero.length; i++){
	     	if(tablero[i][i].equals("0")){
	    	    contador0++;
	      	} else if(tablero[i][i].equals("1")){
	      	    contador1++;
	     	}
	    }
	    if(contador0 == 4 || contador1 == 4){
		temporal = true;
	    }
	    contador0 =0;
	    contador1 =0;
	}
	
	if(!temporal){
	    // Verificación contra la diagonal invertida.
	    int k =0;
	    for(int i=2; i>=0; i--){
		if(tablero[k][i].equals("0")){
		    contador0++;
		} else if(tablero[k][i].equals("1")){
		    contador1++;
	    }
		k++;
		if(contador0 == 4 || contador1 == 4){
		    temporal = true;
		}
	    }
	    contador0 =0;
	    contador1 =0;
	}
	
	if(!temporal){
	    // Caso de empate.
	    int a =0;
	    for(int i=0; i < tablero.length; i++){
		for(int j=0; j < tablero[i].length; j++){
		    if(tablero[i][j].equals("1") || tablero[i][j].equals("0"))
			a++;
		}
	    }
	    if(a == 16){ // 4*4 = 16
		temporal = true;
	    }
	}

	if(temporal)
	    return true;
	else
	    return false;
    }
    
    /**
     * Método toString para imprimir el tablero.
     * @return String -- tablero.
     */
    public String toString(){
	String acc = getLinea()+"\n";
	for(int i=0; i<tablero.length; i++){
	    acc+="| ";
	    for(int j=0; j<tablero[i].length; j++)
		acc += tablero[i][j]+" | ";
	        acc +="\n"+getLinea()+"\n";
	}
	return acc;
    }

     /*
     * Método para obtener una linea.
     * @return String -- Línea.
     */
    private String getLinea(){
	return "-----------------";
    }
        
    /**
     * <p>Método para saber si se puede colocar una ficha en una
     * casilla del tablero.</p>
     * @return boolean -- <code>True</code> o <code>False</code>.
     */
    public boolean puedeColocar(int columna){
	int contador0 = 0;
	int contador1 = 0;
	boolean temporal = false;
	
	if(!temporal){
	    for(int i = 0; i < tablero.length; i++){
		if(!this.tablero[i][columna].equals("0")){
		    temporal = true;
		} else if(!this.tablero[i][columna].equals("1")){
		    temporal = true;
		}
	    }
	}
	
	if(temporal)
	    return true;
	else
	    return false;
    }
    
    /**
     * <p>Método que devuelve al ganador en este juego.</p>
     * @param jugador1 -- <code>Jugador</code>.
     * @param jugador2 -- <code>Jugador</code>.
     * @return String  -- <code>Jugador</code> o <code>null</code>.
     */
    @Override public String ganador(Jugador jugador1, Jugador jugador2){
	if(jugador1.getDia3()){
	    return "El ganador es: "+ jugador1.getNombre();
	} else if(jugador2.getDia3()){
	    return "El ganador es: "+ jugador2.getNombre(); 
	} else
	    return null;
    }
    
    /**
     * <p>Método booleano que devuelve <code>True</code> en
     *  caso de que haya un espacio en blanco en esa celda y 
     * <code>False</code> en caso contrario.</p>
     * @return boolean -- <code>True</code> o <code>False</code>.
     */
    public boolean quienEstaAhi(int fila, int columna){
	String quien = tablero[fila][columna];
	if(quien.equals(" ")){
	    return true;
	} else{
	    return false;
	}
    }
}
