import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Random;
/**
 * <p>Clase que ejecuta el <code>Juego</code> del
 * día número 3<p>.
 * @author Adrián Aguilera Moreno.
 * <p> Creado el 15/02/2021 </p>
 * @version 1.0
 */

public class Dia3{
    
    public static void main(String [] pps){
	
	int opcion = 0;
	Jugador a1 = null;
	Jugador a2 = null;
	boolean terminar = false;
	Object[] dia = new Object[0];
	Conecta4 juego = new Conecta4();
	Object[] jugadores = new Object[0];
	Utilidades util = new Utilidades();
	Utilidades dia3 = new Utilidades();
	Scanner in = new Scanner(System.in);
	Scanner on = new Scanner(System.in);
	jugadores = util.leerObjetos("./Archivos/Dia2.txt");
	//System.out.println(jugadores.length);
	
	dia = dia3.leerObjetos("./Archivos/Dia3.txt");
	for(int i=0; i< dia.length; i++){
	    if(dia[i] instanceof Boolean){
		terminar = true;
	    }
	}
	Pareja [] parejas  = new Pareja[(int)  Math.ceil((0.33)* (jugadores.length - 1))];  //porcentaje2
	 Jugador [] arreglo = new Jugador[(int) Math.ceil((0.66)* (jugadores.length - 1))]; //porcentaje3
	if(arreglo.length != 0){
	    int numerito = 0;
	    // Separar jugadores:
	    for(int i=0; i < jugadores.length; i++){
		if(jugadores[i] instanceof Jugador){
		    //System.out.println(numerito);
		    arreglo[numerito++] = (Jugador) jugadores[i]; 
		}
	    }
	}
	// Revolver jugadores:
	for(int i=0; i < arreglo.length; i++){
	    int numero
		= (int) Math.round(Math.random() * (arreglo.length -1) + 0.5);
	    Jugador temporal = (Jugador) arreglo[numero];
	    arreglo[numero] = arreglo[i];
	    arreglo[i] = temporal;
	}
	
	int kp = 0;
	Pareja p = null;
	// Crear parejas:
	for(int i=0; (i < parejas.length) && (kp < arreglo.length); i++){
	    if(arreglo.length % 2 == 0){
		a1 = arreglo[kp++];
		a2 = arreglo[kp++];
		p = new Pareja1(a1, a2);
		parejas[i] = p;
	    } else{
		if(kp +1 != arreglo.length){
		    a1 = arreglo[kp++];
		    a2 = arreglo[kp++];
		    p = new Pareja1(a1, a2);
		    parejas[i] = p;
		} else{
		    a1 = arreglo[kp++];
		    p = new Pareja1(a1);
		    parejas[i] = p;
		}
	    }
	    p = null;
	    a1 = null;
	    a2 = null;
	}
	
	System.out.println("\n"+"------------------------------------------------------");
	System.out.println("        Bienvenido al día 3 en este torneo.");
	System.out.println("                      Conecta 4\n");
	System.out.println("A continuación se presentan las opciones:");
	System.out.println("1. Empezar el enfrentamiento.");
	System.out.println("2. Presentar las parejas.");
	System.out.println("3. Salir");

	do{
	    do{
		System.out.print("\n Elige una opción: ");
                try{                                                                                        
                    opcion = in.nextInt();
                    in.nextLine();
                } catch(InputMismatchException e){
                    System.out.println();
                    System.out.println("\nDebes introducir un número entre 1 y 3.\n");
                    in.nextLine();
                }
            } while(opcion < 1 || opcion > 3);
	    
	    switch(opcion){
	    case 1:
		if(!terminar){ // if 1
		    for(int i=0; i < parejas.length; i++){ // for 1
			//do{ // Clave1-Clave2
			    String leer = "";
			    do{				    
				System.out.print("Introduzca las claves: ");
				leer = on.nextLine();
				leer = leer.trim();
				if(leer.length() != 21)
				    System.out.println("Debe introducir las claves separadas"+
						       " por un guión.");
			    }while(leer.length() != 21);
			    String c1 = leer.substring(0, 10);
			    String c2 = leer.substring(11);
			    Pareja p1 = null;
			    for(int j=0; j < parejas.length; j++){// for 2
				p1 = parejas[j];
				if(p1 != null){
				    a1 = (Jugador) p1.getJugador(0);
				    a2 = (Jugador) p1.getJugador(1);
				    if(a1.getClave().equals(c1)
				       && a2.getClave().equals(c2)){
					if(a2.getNombre().equals("Ordenador")){
					    int n = 0;
					    // Jugar contra el ordenador.
					    //***************************************************************************
					    do{
					    	int secretoFila = (int) Math.round(Math.random() * 3 + 0.5);
						if(n % 2 == 0){// Inicia el jugador 1.
						    n++;
						    System.out.println("Juega: "+
								       a1.getNombre());
						    int lector = 0;
			     				do{
			     				    System.out.println("\nIntroduzca el número de columna:");
			     				    try{
			     					lector = in.nextInt();
								in.nextLine();
			    				    } catch(Exception e){
			     					System.out.println();
			     					System.out.println("\nDebes introducir un número entre 1 y 4.\n");
			     					in.nextLine();
			     				    }
			     				} while(lector < 1 && lector > 4);
							if(juego.puedeColocar(lector -1)){
							    if(juego.quienEstaAhi(3, (lector -1))){
								juego.colocarTablero("0", (lector -1), 3);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a1.morePuntaje();
			     					    a1.setDia3();
			     					    System.out.println(juego.ganador(a1, a2));
			     					}
							    } else if(juego.quienEstaAhi(2, (lector -1))){
								juego.colocarTablero("0", (lector -1), 2);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a1.morePuntaje();
			     					    a1.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					}
							    } else if(juego.quienEstaAhi(1, (lector -1))){
								juego.colocarTablero("0", (lector -1), 1);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a1.morePuntaje();
			     					    a1.setDia3();
								     System.out.println(juego.ganador(a1, a2));
								     //juego.ganador(a1, a2);
			     					}
							    } else if(juego.quienEstaAhi(0, (lector -1))){
								juego.colocarTablero("0", (lector -1), 0);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a1.morePuntaje();
			     					    a1.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					    //juego.ganador(a1, a2);
			     					}
							    }
							} else{
							    System.out.println("Esa columna  ya está llena.");
							}
							// if: Inicia el jugador 1.
						} else if(n % 2 == 1){// Inicia el jugador Ordenador.
						    n++;
						    System.out.println("Juega: "+ a2.getNombre());
						    if(juego.puedeColocar(secretoFila)){
							if(juego.quienEstaAhi(3, secretoFila)){
							    juego.colocarTablero("1", secretoFila, 3);
							    System.out.println(juego.toString());
							    if(juego.fin()){
								a1.morePuntaje();
								a1.setDia3();
								System.out.println(juego.ganador(a1, a2));
								//juego.ganador(a1, a2);
							    }
							} else if(juego.quienEstaAhi(2, secretoFila)){
							    juego.colocarTablero("1", secretoFila, 2);
							    System.out.println(juego.toString());
							    if(juego.fin()){
								a1.morePuntaje();
								a1.setDia3();
								System.out.println(juego.ganador(a1, a2));
								//juego.ganador(a1, a2);
							    }
							} else if(juego.quienEstaAhi(1, secretoFila)){
							    juego.colocarTablero("1", secretoFila, 1);
							    System.out.println(juego.toString());
							    if(juego.fin()){
								a1.morePuntaje();
								a1.setDia3();
								System.out.println(juego.ganador(a1, a2));
								//juego.ganador(a1, a2);
							    }
							} else if(juego.quienEstaAhi(0, secretoFila)){
							    juego.colocarTablero("1", secretoFila, 0);
							    System.out.println(juego.toString());
							    if(juego.fin()){
								a1.morePuntaje();
								a1.setDia3();
								System.out.println(juego.ganador(a1, a2));
								//juego.ganador(a1, a2);
							    }
							}	    
						    }
						}
					    }while(!juego.fin());
					    // Fin del juego impar.
					    //***************************************************************************
					} else{
					    int n = 0;
					    do{
					    	//int secretoFila = (int) Math.round(Math.random() * 3 + 0.5);
						if(n % 2 == 0){// Inicia el jugador 1.
						    n++;
						    System.out.println("Juega: "+
								       a1.getNombre());
						    int lector = 0;
			     				do{
			     				    System.out.println("\nIntroduzca el número de columna:");
			     				    try{
			     					lector = in.nextInt();
								in.nextLine();
			    				    } catch(Exception e){
			     					System.out.println();
			     					System.out.println("\nDebes introducir un número entre 1 y 4.\n");
			     					in.nextLine();
			     				    }
			     				} while(lector < 1 && lector > 4);
							if(juego.puedeColocar(lector -1)){
							    if(juego.quienEstaAhi(3, (lector -1))){
								juego.colocarTablero("0", (lector -1), 3);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a1.morePuntaje();
			     					    a1.setDia3();
								    System.out.println(juego.ganador(a1, a2));
								    //juego.ganador(a1, a2);
			     					}
							    } else if(juego.quienEstaAhi(2, (lector -1))){
								juego.colocarTablero("0", (lector -1), 2);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a1.morePuntaje();
			     					    a1.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					    //juego.ganador(a1, a2);
			     					}
							    } else if(juego.quienEstaAhi(1, (lector -1))){
								juego.colocarTablero("0", (lector -1), 1);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a1.morePuntaje();
			     					    a1.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					    //juego.ganador(a1, a2);
			     					}
							    } else if(juego.quienEstaAhi(0, (lector -1))){
								juego.colocarTablero("0", (lector -1), 0);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a1.morePuntaje();
			     					    a1.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					    //juego.ganador(a1, a2);
			     					}
							    }
							} else{
							    System.out.println("Esa columna  ya está llena.");
							}
							// if: Inicia el jugador 1.
						} else if(n % 2 == 1){// Inicia el jugador 2.
						    n++;
						    System.out.println("Juega: "+
								       a2.getNombre());
						    int lector = 0;
			     				do{
			     				    System.out.println("\nIntroduzca el número de columna:");
			     				    try{
			     					lector = in.nextInt();
								in.nextLine();
			    				    } catch(Exception e){
			     					System.out.println();
			     					System.out.println("\nDebes introducir un número entre 1 y 4.\n");
			     					in.nextLine();
			     				    }
			     				} while(lector < 1 && lector > 4);
							if(juego.puedeColocar(lector -1)){
							    if(juego.quienEstaAhi(3, (lector -1))){
								juego.colocarTablero("1", (lector -1), 3);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a2.morePuntaje();
			     					    a2.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					    //juego.ganador(a1, a2);
			     					}
							    } else if(juego.quienEstaAhi(2, (lector -1))){
								juego.colocarTablero("1", (lector -1), 2);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a2.morePuntaje();
			     					    a2.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					    //juego.ganador(a1, a2);
			     					}
							    } else if(juego.quienEstaAhi(1, (lector -1))){
								juego.colocarTablero("1", (lector -1), 1);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a2.morePuntaje();
			     					    a2.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					    //juego.ganador(a1, a2);
			     					}
							    } else if(juego.quienEstaAhi(0, (lector -1))){
								juego.colocarTablero("1", (lector -1), 0);
								System.out.println(juego.toString());
			     					if(juego.fin()){
			     					    a2.morePuntaje();
			     					    a2.setDia3();
								    System.out.println(juego.ganador(a1, a2));
			     					    //juego.ganador(a1, a2);
			     					}
							    }
							} else{
							    System.out.println("Esa columna  ya está llena.");
							}
						}
					    }while(!juego.fin());
					}// Fin del juego par.
				    }
				} // for 2
			    }//boolean parar = false;
		    }// for 1
		} else{
		    System.out.println("Ha terminado esta etapa, consulte el manual.");
		}
		break;
		//****************************FIN del caso 1*********************************		
	    case 2:
		for(int i=0; i<parejas.length; i++){
		    Pareja p1 = (Pareja) parejas[i];
		    if(p1 != null){
			System.out.println(p1.toString());
		    }
		}
		break;
		//****************************FIN del caso 2*********************************
	    case 3:
		//if(!terminar){
		if(!terminar){                                                                                                
		    Pareja pareja = null;                                                                                         
		    for(int i=0; i < parejas.length; i++){                                                                        
			if(parejas[i] != null){
			    pareja = (Pareja) parejas[i];                                                                             
			    dia = dia3.agrupar(pareja);                                                                               
			    Jugador j1 = (Jugador) pareja.getJugador(0);                                                              
			    Jugador j2 = (Jugador) pareja.getJugador(1);                                                              
			    if(j2.getNombre().equals("Ordenador")){                                                                   
				dia = dia3.agrupar(j1);                                                                               
			    } else{                                                                                                   
				dia = dia3.agrupar(j1);                                                                               
				dia = dia3.agrupar(j2);                                                                               
			    }
			}
		    }                                                                                                             
		    System.out.println("\n      *** Ha decidido terminar esta etapa. ***");                                       
		    terminar = true;                                                                                              
		    dia = dia3.agrupar((Boolean) terminar);                                                                       
		    dia3.escribirObjetos("./Archivos/Dia3.txt", dia);                                                             
                } else{                                                                                                       
                    System.out.println("Adios ...");                                                                          
		}
		break;
		//****************************FIN del caso 3*********************************
	    default:
		System.out.println("Up's algo falló.");
		break;
		//****************************FIN del caso DEFAULT*********************************
	    }	    
	}while(opcion != 3);// Condición.
    }
}
