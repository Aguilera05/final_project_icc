import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * Clase que genera registros de tipos de la clase <code>Jugador</code>.
 * @author Adrian Aguilera Moreno.
 * <p>Created on 29/01/2021</p>
 * @version 1.0
 */

public class Registros{
    
    public static void main(String [] pps){
	
	int opcion = 0;
	boolean terminar = false;
	Scanner in = new Scanner(System.in);
	Scanner on = new Scanner(System.in);
	Object[] jugadores = new Object[0];
	Utilidades util = new Utilidades();
	jugadores = util.leerObjetos("./Archivos/Registros.txt");
	
	for(int i=0; i< jugadores.length; i++){
	    if(jugadores[i] instanceof Boolean){
		terminar = true;
	    }
	}
	
	System.out.println("\n"+"--------------------------------------------------------");
	System.out.println("   Bienvenido al torneo de la Facultad de Ciencias"+"\n");
	do{
	    System.out.println("A continuación se presentan las opciones:");
	    // Menú de opciones para el registro.
	    System.out.println("1. Ingresar un jugador.");
	    System.out.println("2. Mostrar los jugadores registrados al momento.");
	    System.out.println("3. Salir del registro.");
	    System.out.println("4. Terminar el registro.");
	    // Verificar que el usuario ingrese un número entero y dentrolos parámetros [1, 4].
	    do{
		System.out.print("\n Elige una opción: ");
		try{
		    opcion = in.nextInt();
		    in.nextLine();
		} catch(Exception e){
		    System.out.println();
		    System.out.println("Debes introducir un número entre 1 y 4.");
		    in.nextLine();
		}
	    }while(opcion < 1 || opcion > 4);
	    // Realizar dependiendo de la opción seleccionada.
	    switch(opcion){
	    case 1:
		if(!terminar){
		System.out.print("Introduce el nombre del jugador: ");
		String nombre = on.nextLine();
		int d = 0;
		String dia;
		do{
		    System.out.print("Introduce el día de nacimiento: ");
		    dia = on.nextLine();
		    d = dia.length();
		} while(d != 2);
		int m = 0;
		String mes;
		do{
		    System.out.print("Introduce el mes de nacimiento: ");
		    mes = on.nextLine();
		    m = mes.length();
		} while(m != 2);
		int a = 0;
		String anio;
		do{
		    System.out.print("Introduce el año de nacimiento: ");
		    anio = on.nextLine();
		    a = anio.length();
		} while(a != 4);
		String fN = dia +"/"+ mes +"/"+ anio;
		System.out.print("Introduce el sexo del jugador: ");
		String sexo = on.nextLine();
		System.out.print("Introduce la carrera de origen: ");
		String carrera = on.nextLine();
		Jugador nuevo = new Jugador(nombre, fN, sexo, carrera);
		jugadores = util.agrupar(nuevo);
		} else{
		    System.out.println("\nUsted a finalizado está etapa, consulte el manual.\n");
		}
		break;
	    case 2:
		System.out.println("Número de jugadores: "+ jugadores.length);
		Jugador cambio;
		for(int i=0; i < jugadores.length; i++){
		    if(jugadores[i] instanceof Jugador){
			cambio = (Jugador) jugadores[i];
			System.out.println(cambio.toString());
		    }
		}
		break;
	    case 3:
		
		System.out.println("Ha decidido salir del registro.");
		if(!terminar){
		    util.escribirObjetos("./Archivos/Registros.txt", jugadores);
		}
		break;
	    case 4:
		//desicion = new Boolean("true");
		if(!terminar){
		    terminar = true;
		    jugadores = util.agrupar((Boolean) terminar);
		    System.out.println("Ha decidido terminar el registro.");
		    util.escribirObjetos("./Archivos/Registros.txt", jugadores);
		} else{
		    System.out.println("Adios...");
		}
		break;
	    default:
		System.out.println("Up's, saliendo ...");
		break;		
	    }
	    // Terminar el ciclo en caso requerido, de lo contrario repetir.
	}while(opcion != 3 && opcion != 4);
	System.out.println("\n"+"--------------------------------------------------------\n");
    }
}
