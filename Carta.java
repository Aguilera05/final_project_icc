/**
 * <p>Clase que simula el comportamiento de una
 * <code>Carta</code>.</p>
 * @author Adrian Aguilera Moreno.
 * <p> Creado el 01/02/2021 </p>
 * @version 1.0
 */

public class Carta{
    
    /* Atributos */
    private int valorCarta;
    private String nombreCarta;
    
    /**
     * <p>Método constructor por parámetros que
     * construye una <code>Carta</code> por medio
     *  de su nombre asignado.</p>
     * @param nombre -- String: Nombre de la carta.
     */
    public Carta(String nombre){
	if(nombre.equalsIgnoreCase("J")){
	    nombreCarta = "J";
	    this.valorCarta = 10;
	} else if(nombre.equalsIgnoreCase("K")){
	    nombreCarta = "K";
	    this.valorCarta = 10;
	} else if(nombre.equalsIgnoreCase("Q")){
	    nombreCarta = "Q";
	    this.valorCarta = 10;
	} else{
	    nombreCarta = null;
	    this.valorCarta = 0;
	}
    }
    
    /**
     * <p>Método constructor por parámetros, sólo
     * debe usarse en caso de crear un As como
     * <code>Carta</code>.</p>
     * @param As -- int: Valor de la carta.
     */
    public Carta(int As){
	this.nombreCarta = "As";
	this.valorCarta = As;
    }
    
    /**
     * <p>Método modificador que asigna un nombre a
     * la carta que instancía el método.</p>
     * @param nombre -- String: nombre de la 
     *                  <code>Carta</code>.
     */
    public void setNombreCarta(String nombre){
	this.nombreCarta = nombre;
    }
    
    /**
     * <p>Método inspector que devuelve el nombre
     * de la carta que instancía el método.</p>
     * @return String -- Nombre de la 
     *                  <code>Carta</code>.
     */
    public String getNombreCarta(){
	return this.nombreCarta;
    }
    
    /**
     * <p>Método modificador que asigna un valor a
     * la <code>Carta</code> que instancía en el 
     * método.</p>
     * @param valor -- int: Valor a sustituir.
     */
    public void setValor(int valor){
	this.valorCarta = valor;
    }

    /**
     * <p>Método inspector que obtiene el valor numérico
     * de la <code>Carta</code>.</p>
     * @return int -- Valor obtenido.
     */
    public int getValor(){
	return this.valorCarta;
    }
    
    /**
     * <p>Método que imprime una carta en forma de String.</p>
     * @return String -- <code>Carta</code> en forma de String.
     */
    @Override public String toString(){
	String a = this.getNombreCarta();
	String b = String.valueOf(this.getValor());
	String l = "-----------";
	return "\n\n"+ l +"\n"+ a +"\n"+
	    l +"\n"+ b +"\n"+ l +"\n\n";
    }
    
     /**
     * <p>Método que devuelve <code>True</code> o 
     * <code>False</code> si una <code>Carta</code>
     * es igual a otra o no respectivamente.</p>
     * @param aComparar -- Objeto de tipo 
     *        <code>Carta</code> que se compara
     *        con la <code>Carta</code> que instancía
     *        al método.
     * @return boolean -- <code>True</code> o 
     *                    <code>False</code>.
     */ 
    public boolean equals(Carta aComparar){
	boolean a, b;
	if(this.getValor() == aComparar.getValor())
	    a = true;
	else
	    a = false;
	if(this.getNombreCarta().equals(aComparar.getNombreCarta()))
	    b = true;
	else
	    b = false;
		
	if(a && b)
	    return true;
	else
	    return false;
    }
}
