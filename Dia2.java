import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Random;

/**
 * Clase que ejecuta el <code>Dia</code> número 1
 * de <code>Juegos</code>.
 * @author Adrian Aguilera Moreno.
 * <p> Creado el 13/02/2021 </p>
 * @version 1.4
 */

public class Dia2{
    
    public static void main(String [] pps){
	
	 int opcion = 0;
	 Jugador b1 = null;
	 Jugador b2 = null;
	 Jugador a1 = null;
	 Jugador a2 = null;
	 Jugador nuevo = null;
	 boolean terminar = false;
	 Object[] dia = new Object[0];
	 Object[] jugadores = new Object[0];
	 Utilidades util = new Utilidades();
	 Utilidades dia2 = new Utilidades();
	 Scanner in = new Scanner(System.in);
	 Scanner on = new Scanner(System.in);
	 jugadores = util.leerObjetos("./Archivos/Dia1.txt");
	 //System.out.println(jugadores.length);
	 
	 dia = dia2.leerObjetos("./Archivos/Dia2.txt");
	 for(int i=0; i< dia.length; i++){
	     if(dia[i] instanceof Boolean){
		 terminar = true;
	     }
	 }
	 //**********************************************************************************
	 Pareja [] parejas  = new Pareja[(int)  Math.ceil((0.33)* (jugadores.length - 1))]; 
	 Jugador [] arreglo = new Jugador[(int) Math.ceil((0.66)* (jugadores.length - 1))]; 
	 if(arreglo.length != 0){
	     int numerito = 0;
	     // Separar jugadores:
	     for(int i=0; i < jugadores.length; i++){
		 if(jugadores[i] instanceof Jugador){
		     //System.out.println(numerito);
		     arreglo[numerito++] = (Jugador) jugadores[i]; 
		 }
	     }
	 }
	 // Revolver jugadores:
	 for(int i=0; i < arreglo.length; i++){
	     int numero
		 = (int) Math.round(Math.random() * (arreglo.length -1) + 0.5);
	     Jugador temporal = (Jugador) arreglo[numero];
	     arreglo[numero] = arreglo[i];
	     arreglo[i] = temporal;
	 }
	 
	 int kp = 0;
	 Pareja p = null;
	 // Crear parejas:
	 for(int i=0; (i < parejas.length) && (kp < arreglo.length); i++){
	     if(arreglo.length % 2 == 0){
		 a1 = arreglo[kp++];
		 a2 = arreglo[kp++];
		 p = new Pareja1(a1, a2);
		 parejas[i] = p;
	     } else{
		 if(kp +1 != arreglo.length){
		     a1 = arreglo[kp++];
		     a2 = arreglo[kp++];
		     p = new Pareja1(a1, a2);
		     parejas[i] = p;
		 } else{
		     a1 = arreglo[kp++];
		     p = new Pareja1(a1);
		     parejas[i] = p;
		 }
	     }
	     a1 = null;
	     a2 = null;
	 }
	System.out.println("\n"+"------------------------------------------------------");
	System.out.println("        Bienvenido al día 2 en este torneo.");
	System.out.println("                       Dados\n");
	System.out.println("A continuación se presentan las opciones:");
	System.out.println("1. Empezar el enfrentamiento.");
	System.out.println("2. Presentar las parejas.");
	System.out.println("3. Salir");
	
	do{
	    do{
		System.out.print("\n Elige una opción: ");
                try{  
                    opcion = in.nextInt();
                    in.nextLine();
                } catch(Exception e){
                    System.out.println();
                    System.out.println("\nDebes introducir un número entre 1 y 3.\n");
                    in.nextLine();
                }
            } while(opcion < 1 || opcion > 3);
	    // Que hacer dada la opción:
	    switch(opcion){
	    case 1:
		int h=0;
		int k=0;
		for(int i=0; i<parejas.length; i++){
		    if(parejas[i] != null){
			k++;
		    }
		}
		// Código para empezar los enfrentamientos.
		if(!terminar){
		    a1 = null;
		    a2 = null;
		    b1 = null;
		    b2 = null;
		    int lector = 0;
		    String leer = "";
		    String sale = ""; // Guardar la coordenada del dado.
		    int contador1 = 0;
		    int contador2 = 0;
		    boolean salir = false;
		    boolean saber = false;		  
		    int contadorAntes1 = 0;
		    int contadorAntes2 = 0;
		    boolean repetir = false;
		    boolean alguienGano = false;
		    JugarDados juego = new JugarDados();
		    for(int i=0; i < k; i++){// Aquí modifique
			//h++;
			//if(parejas[i] != null){//------------------------------------------------------------------------------
			do{// Clave-Clave
			    System.out.print("Introduzca las claves: ");
			    leer = on.nextLine();
			    leer = leer.trim();
			}while(leer.length() != 21);
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			String c1 = leer.substring(0, 10);                                             
			String c2 = leer.substring(11);                                                
			for(int m=0; m < parejas.length; m++){                                         
			    if(parejas[m] != null){
			    a1 = parejas[m].getJugador(0);
			    a2 = parejas[m].getJugador(1);
			    
			    saber = (a1.getDia2() && a2.getDia2());           
			    if(a1.getClave().equals(c1)                                                
			       && a2.getClave().equals(c2)
			       && !saber){
				b1 = a1;
				b2 = a2;
			    }
			    }
		    }
			int dado1 = 0;
			int dado2 = 0;
			if(!saber){			    
			    if(b2.getNombre().equals("Ordenador")){
				// Código para jugar contra el computador.
				//.................................................................................................
				int n =0;
				do{				    
				    if(n % 2 == 0){
					contador1 = 0; //Contenedor antes y otro después.
					do{
					    System.out.println("Juega: "+ b1.getNombre()
							       +", Clave: "+b1.getClave());
					    System.out.println("Opciones: ");                                         
					    System.out.println("1. tirar.");                                                     
					    System.out.print("\n Elige una opción: ");
					    try{
						lector = in.nextInt();
						in.nextLine();                                                   
					    } catch(Exception e){                                                 
						System.out.println();                                             
						System.out.println("\nDebes introducir 1.\n");
						in.nextLine();                                                    
					    }
					    if(repetir){
						System.out.println("Debe repetir su tiro."); 
					    }
					} while(lector != 1);
					String temporal = juego.tirarDados();
					dado1 = Integer.parseInt(temporal.substring(0, 1));
					dado2 = Integer.parseInt(temporal.substring(2));
					contador1 = dado1 + dado2;
					System.out.println("Se tiró: "+ temporal);
					if((contador1 != 2) && (contador1 != 3) && (contador1 != 12)){
					    if(contador1 == contadorAntes1){
						// Gana b1.
						b1.setDia2();
						b2.setDia2();
						b1.morePuntaje();
						alguienGano = true;
						System.out.println("El ganador es: "+ b1.getNombre());
					    }
					    if((contador1 == 7) || (contador1 == 11)){
						// Gana b1.						
						b1.setDia2();
						b2.setDia2();
						b1.morePuntaje();
						alguienGano = true;
						System.out.println("El ganador es: "+ b1.getNombre());
					    }
					    if((contador1 == 4) || (contador1 == 5) || (contador1 == 6) || (contador1 == 8)
					       || (contador1 == 9) || (contador1 == 10)){
						n--;
						repetir = true;
						if(contadorAntes1 == contador1){
						    //Gana b1.
						    n++;
						    b1.setDia2();
						    b2.setDia2();
						    repetir = false;
						    b1.morePuntaje();
						    alguienGano = true;
						    System.out.println("El ganador es: "+ b1.getNombre());
						}
					    }
					} else{
					    // Gana b2, pues b1 pierde.
					    b1.setDia2();
					    b2.setDia2();
					    b2.morePuntaje();
					    alguienGano = true;
					    System.out.println("El ganador es: "+ b2.getNombre());
					}
					contadorAntes1 = dado1 + dado2;
				    } else if(n % 2 == 1){
					// Aquí va el código. Para el jugador 2.
					//-------------------------------Jugador2-----------------------------------
					contador2 = 0; //Contenedor antes y otro después.
					String temporal = juego.tirarDados();
					dado1 = Integer.parseInt(temporal.substring(0, 1));
					dado2 = Integer.parseInt(temporal.substring(2));
					contador2 = dado1 + dado2;
					System.out.println("Se tiró: "+ temporal);
					if((contador2 != 2) && (contador2 != 3) && (contador2 != 12)){
					    if(contador2 == contadorAntes2){
						// Gana b2.
						b1.setDia2();
						b2.setDia2();
						b2.morePuntaje();
						alguienGano = true;
						System.out.println("El ganador es: "+ b2.getNombre());
					    }
					    if((contador2 == 7) || (contador2 == 11)){
						// Gana b1.						
						b1.setDia2();
						b2.setDia2();
						b2.morePuntaje();
						alguienGano = true;
						System.out.println("El ganador es: "+ b2.getNombre());
					    }
					    if((contador2 == 4) || (contador2 == 5) || (contador2 == 6) || (contador2 == 8)
					       || (contador2 == 9) || (contador2 == 10)){
						n--;
						repetir = true;
						if(contadorAntes2 == contador2){
						    //Gana b1.
						    n++;
						    b1.setDia2();
						    b2.setDia2();
						    repetir = false;
						    b2.morePuntaje();
						    alguienGano = true;
						    System.out.println("El ganador es: "+ b2.getNombre());
						}
					    }
					} else{
					    // Gana b2, pues b1 pierde.
					    b1.setDia2();
					    b2.setDia2();
					    b1.morePuntaje();
					    alguienGano = true;
					    System.out.println("El ganador es: "+ b1.getNombre());
					}
					//-------------------------------Jugador2-----------------------------------
				    }
				    n++;
				}while(!alguienGano);				
				//.................................................................................................
			    } else{
				int n =0;
				do{				    
				    if(n % 2 == 0){
					contador1 = 0; //Contenedor antes y otro después.
					do{
					    System.out.println("Juega: "+ b1.getNombre()
							       +", Clave: "+b1.getClave());
					    System.out.println("Opciones: ");                                         
					    System.out.println("1. tirar.");                                                     
					    System.out.print("\n Elige una opción: ");
					    try{
						lector = in.nextInt();
						in.nextLine();                                                   
					    } catch(Exception e){                                                 
						System.out.println();                                             
						System.out.println("\nDebes introducir 1.\n");
						in.nextLine();                                                    
					    }
					    if(repetir){
						System.out.println("Debe repetir su tiro."); 
					    }
					} while(lector != 1);
					String temporal = juego.tirarDados();
					dado1 = Integer.parseInt(temporal.substring(0, 1));
					dado2 = Integer.parseInt(temporal.substring(2));
					contador1 = dado1 + dado2;
					System.out.println("Se tiró: "+ temporal);
					if((contador1 != 2) && (contador1 != 3) && (contador1 != 12)){
					    if(contador1 == contadorAntes1){
						// Gana b1.
						b1.setDia2();
						b2.setDia2();
						b1.morePuntaje();
						alguienGano = true;
						System.out.println("El ganador es: "+ b1.getNombre());
					    }
					    if((contador1 == 7) || (contador1 == 11)){
						// Gana b1.						
						b1.setDia2();
						b2.setDia2();
						b1.morePuntaje();
						alguienGano = true;
						System.out.println("El ganador es: "+ b1.getNombre());
					    }
					    if((contador1 == 4) || (contador1 == 5) || (contador1 == 6) || (contador1 == 8)
					       || (contador1 == 9) || (contador1 == 10)){
						n--;
						repetir = true;
						if(contadorAntes1 == contador1){
						    //Gana b1.
						    n++;
						    b1.setDia2();
						    b2.setDia2();
						    repetir = false;
						    b1.morePuntaje();
						    alguienGano = true;
						    System.out.println("El ganador es: "+ b1.getNombre());
						}
					    }
					} else{
					    // Gana b2, pues b1 pierde.
					    b1.setDia2();
					    b2.setDia2();
					    b2.morePuntaje();
					    alguienGano = true;
					    System.out.println("El ganador es: "+ b2.getNombre());
					}
					contadorAntes1 = dado1 + dado2;
				    } else if(n % 2 == 1){
					// Aquí va el código. Para el jugador 2.
					//-------------------------------Jugador2-----------------------------------
					contador2 = 0; //Contenedor antes y otro después.
					do{
					    System.out.println("Juega: "+ b2.getNombre()
							       +", Clave: "+b2.getClave());
					    System.out.println("Opciones: ");                                         
					    System.out.println("1. tirar.");                                                     
						System.out.print("\n Elige una opción: ");
						try{
						    lector = in.nextInt();
						    in.nextLine();                                                   
						} catch(Exception e){                                                 
						    System.out.println();                                             
						    System.out.println("\nDebes introducir 1.\n");
						    in.nextLine();                                                    
						}
						if(repetir){
						    System.out.println("Debe repetir su tiro."); 
						}
					    } while(lector != 1);
					String temporal = juego.tirarDados();
					dado1 = Integer.parseInt(temporal.substring(0, 1));
					dado2 = Integer.parseInt(temporal.substring(2));
					contador2 = dado1 + dado2;
					System.out.println("Se tiró: "+ temporal);
					if((contador2 != 2) && (contador2 != 3) && (contador2 != 12)){
					    if(contador2 == contadorAntes2){
						// Gana b2.
						b1.setDia2();
						b2.setDia2();
						b2.morePuntaje();
						alguienGano = true;
						System.out.println("El ganador es: "+ b2.getNombre());
					    }
					    if((contador2 == 7) || (contador2 == 11)){
						// Gana b1.						
						b1.setDia2();
						b2.setDia2();
						b2.morePuntaje();
						alguienGano = true;
						System.out.println("El ganador es: "+ b2.getNombre());
					    }
					    if((contador2 == 4) || (contador2 == 5) || (contador2 == 6) || (contador2 == 8)
					       || (contador2 == 9) || (contador2 == 10)){
						n--;
						repetir = true;
						if(contadorAntes2 == contador2){
						    //Gana b1.
						    n++;
						    b1.setDia2();
						    b2.setDia2();
						    repetir = false;
						    b2.morePuntaje();
						    alguienGano = true;
						    System.out.println("El ganador es: "+ b2.getNombre());
						}
					    }
					} else{
					    // Gana b2, pues b1 pierde.
					    b1.setDia2();
					    b2.setDia2();
					    b1.morePuntaje();
					    alguienGano = true;
					    System.out.println("El ganador es: "+ b1.getNombre());
					}
					//-------------------------------Jugador2-----------------------------------
				    }
				    n++;
				}while(!alguienGano);				
			    }
			} else{
			    System.out.println("Los jugadores ya han pasado este torneo");
			    //}
			}
		    }		
		    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		} else{
		    System.out.println("\nUsted a finalizado esta etapa, consulte el manual.");
		}
		break;
	    case 2:
		// Código para presentar las parejas.
		p = null;
		for(int i=0; i < parejas.length; i++){
		    p = (Pareja) parejas[i];
		    if(p != null){
			//System.out.println(parejas[i]);
		    
		    System.out.println(p.toString());
		    }
		}
		break;
	    case 3:
		// Aquí va el código para escribir en "Dia2.txt".
		if(!terminar){                                                                                                
                Pareja pareja = null;                                                                                         
                for(int i=0; i < parejas.length; i++){                                                                        
		    if(parejas[i] != null){
		    pareja = (Pareja) parejas[i];                                                                             
                    dia = dia2.agrupar(pareja);                                                                               
                    Jugador j1 = (Jugador) pareja.getJugador(0);                                                              
                    Jugador j2 = (Jugador) pareja.getJugador(1);                                                              
                    if(j2.getNombre().equals("Ordenador")){                                                                   
                        dia = dia2.agrupar(j1);                                                                               
                    } else{                                                                                                   
                        dia = dia2.agrupar(j1);                                                                               
                        dia = dia2.agrupar(j2);                                                                               
                    }
		    }
                }                                                                                                             
                System.out.println("\n      *** Ha decidido terminar esta etapa. ***");                                       
                terminar = true;                                                                                              
                dia = dia2.agrupar((Boolean) terminar);                                                                       
                dia2.escribirObjetos("./Archivos/Dia2.txt", dia);                                                             
                } else{                                                                                                       
                    System.out.println("Adios ...");                                                                          
		 }
		 break;
	    default:
		System.out.println("\n"+"Up's, algo salió mal...\n");
		break;
	    }
	} while(opcion != 3);
	System.out.println("\n"+"------------------------------------------------------\n");
    }
}
