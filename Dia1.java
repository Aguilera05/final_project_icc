import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Random;

/**
 * Clase que ejecuta el <code>Dia</code> número 1
 * de <code>Juegos</code>.
 * @author Adrian Aguilera Moreno.
 * <p> Creado el 11/02/2021 </p>
 * @version 1.4
 */

public class Dia1{
    
    public static void main(String [] pps){

	int opcion = 0;
	Jugador nuevo = null;
	boolean terminar = false;
	Object[] dia = new Object[0];
	Object[] jugadores = new Object[0];
	Utilidades util = new Utilidades();
	Utilidades dia1 = new Utilidades();
	Scanner in = new Scanner(System.in);
	Scanner on = new Scanner(System.in);
	jugadores = util.leerObjetos("./Archivos/Registros.txt");

	dia = dia1.leerObjetos("./Archivos/Dia1.txt");
	for(int i=0; i< dia.length; i++){
	    if(dia[i] instanceof Boolean){
		terminar = true;
	    }
	}
	
	Pareja1[] parejas = new Pareja1[ (int) Math.floor(jugadores.length/2)];
	// Revolver jugadores:
	for(int i=0; i < jugadores.length; i++){
	    int numero
		= (int) Math.round(Math.random() * (jugadores.length -1) + 0.5);
	    if(jugadores[numero] instanceof Jugador){
		Jugador temporal = (Jugador) jugadores[numero];
		jugadores[numero] = jugadores[i];
		jugadores[i] = temporal;
	    }
	    if(jugadores[numero] instanceof Boolean
	       && numero != (jugadores.length -1)){
		boolean tempV = (Boolean) jugadores[numero];
		Jugador tempJ = (Jugador) jugadores[jugadores.length -1];
		jugadores[jugadores.length -1] = tempV;
		jugadores[numero] = tempJ;
	    }
	}
	// Crear parejas para este juego:
	if(jugadores.length % 2 == 1){
	    int j =0, k =0;
	    Pareja1 nueva = null;
	    Jugador a, b;
	    while(k < (jugadores.length -1)){
		a = (Jugador) jugadores[k++];
		b = (Jugador) jugadores[k++];
		nueva = new Pareja1(a, b);
		parejas[j++] = nueva;		
	    }
	} else{
	    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	    int j =0, k =0;
	    Pareja1 nueva = null;
	    Jugador a, b;
	    while(k < (jugadores.length -2)){
		a = (Jugador) jugadores[k++];
		b = (Jugador) jugadores[k++];
		nueva = new Pareja1(a, b);
		parejas[j++] = nueva;		
	    }
	    nuevo = (Jugador) jugadores[(jugadores.length -2)];
	    nueva = new Pareja1(nuevo);
	    parejas[j++] = nueva;
	    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	}
	
	System.out.println("\n"+"------------------------------------------------------");
	System.out.println("        Bienvenido al día 1 en este torneo.");
	System.out.println("                 Mini Blackjack\n");
	System.out.println("A continuación se presentan las opciones:");
	System.out.println("1. Empezar el enfrentamiento.");
	System.out.println("2. Presentar las parejas.");
	System.out.println("3. Salir");
	do{
	    do{
		System.out.print("\n Elige una opción: ");
		try{
		    opcion = in.nextInt();
		    in.nextLine();
		} catch(Exception e){
		    System.out.println();
		    System.out.println("\nDebes introducir un número entre 1 y 3.\n");
		    in.nextLine();
		}
	    } while(opcion < 1 || opcion > 3);
	    // Que hacer dada la opción:
	    switch(opcion){
	    case 1:
		MiniBlackJack juego = new MiniBlackJack();
		Jugador b1 = null;
		Jugador b2 = null;
		boolean saber = false;
		if(!terminar){ // !juego.fin();
		    boolean quedar = false;
		    int n = 0;
		    int lector = 0;
		    int almacen1 = 0;
		    int almacen2 = 0;
		    boolean t = false;
		    boolean q2 = false;
		    boolean q1 = false;
		    boolean salir = false;
		    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		    for(int i=0; i < parejas.length; i++){
			do{ // Clave1-Clave2
			    String leer = "";
			    do{				    
				System.out.print("Introduzca las claves: ");
				leer = on.nextLine();
				leer = leer.trim();
				if(leer.length() != 21)
				    System.out.println("Debe introducir las claves separadas"+
						       " por un guión.");
			    }while(leer.length() != 21);
			    String c1 = leer.substring(0, 10);
			    String c2 = leer.substring(11);
			    for(int m=0; m < parejas.length; m++){
				Jugador a1 = parejas[m].getJugador(0);
				Jugador a2 = parejas[m].getJugador(1);
				saber = ((a1.getPuntaje() == 1) // Se sale en caso de que 
					 ||(a2.getPuntaje() == 1));// la pareja ya halla jugado.
				if(a1.getClave().equals(c1)
				   && a2.getClave().equals(c2)
				   && !saber){
				    b1 = a1;
				    b2 = a2;
				    salir = true;
				}
			    }
			} while(!salir);
			juego.revolverCartas();
			//--------------------------------------------------------
			do{			
			    if((n % 2 == 0) && !q1){
				n++;
				if(q2)
				    n++;
				System.out.println("Juega: "+ b1.getNombre()
						   +", Clave: "+ b1.getClave());
				System.out.println("Opciones: ");
				System.out.println("1. Quedar.");
				System.out.println("2. Pedir carta.");
				do{
				    System.out.print("\n Elige una opción: ");
				    try{
					lector = in.nextInt();
					in.nextLine();
				    } catch(Exception e){
					System.out.println();
					System.out.println("\nDebes introducir un número entre 1 y 2.\n");
					in.nextLine();
				    }
				} while(lector < 1 && lector > 2);
				if(lector == 2){
				    System.out.println("Sale: ");
				    Carta cartaTemporal = (Carta) juego.sacarCarta();
				    System.out.println(cartaTemporal.toString());
				    almacen1 += (int) cartaTemporal.getValor();
				} else if(lector == 1){
				    juego.quedar(b1);
				    q1 = true;
				}
				
			    } else if((n % 2 == 1) && !q2){
				n++;
				if(q1)
				    n++;
				//------------------------------------------------------------
				System.out.println("Juega: "+ b2.getNombre()
						   +", Clave: "+ b2.getClave());
				System.out.println("Opciones: ");
				System.out.println("1. Quedar.");
				System.out.println("2. Pedir carta.");
				do{
				    System.out.print("\n Elige una opción: ");
				    try{
					lector = in.nextInt();
					in.nextLine();
				    } catch(Exception e){
					System.out.println();
					System.out.println("\nDebes introducir un número entre 1 y 2.\n");
					in.nextLine();
				    }
				} while(lector < 1 || lector > 2);
				if(lector == 2){
				    System.out.println("Sale: ");
				    Carta cartaTemporal = (Carta) juego.sacarCarta();
				    System.out.println(cartaTemporal.toString());
				    almacen2 += (int) cartaTemporal.getValor();
				} else if(lector == 1){
				    juego.quedar(b2);
				    q2 = true;
				}
				//------------------------------------------------------------
				
			    }
			    // Quien gana: El que este más cercano a 21 sin pasarse.
			    if(almacen1 >= 21)
				t = true;
			    else if(almacen2 >= 21)
				t = true;
			    quedar = q1 && q2;
			    if(quedar && t)
				System.out.println("\nTerminó la partida.\n");
			} while(!quedar && !t && (n<15)); // Condición.
			// Quien gana: El que este más cercano a 21 sin pasarse.
			if(almacen1 > almacen2 && almacen1 < 22){
			    b1.morePuntaje();
			    System.out.println(" El ganador ha sido "+ b1.getNombre());
			} else if(almacen2 > almacen1 && almacen2 < 22){
			    b2.morePuntaje();
			    System.out.println(" El ganador ha sido "+ b2.getNombre());
			} else if(almacen1 < 22 && almacen2 > 22){
			    b1.morePuntaje();
			    System.out.println(" El ganador ha sido "+ b1.getNombre());
			} else if(almacen2 < 22 && almacen1 > 22){
			    b2.morePuntaje();
			    System.out.println(" El ganador ha sido "+ b2.getNombre());	
			} else{
			    System.out.println(" Up's parece que fue empate.");
			}
		    }
		} else{
		    System.out.println("\nUsted a finalizado esta etapa, consulte el manual.");
		}
		break;
	    case 2:
		System.out.println("Las parejas son: ");
		    Pareja p1;
		    for(int i=0; i < parejas.length; i++){
			p1  = parejas[i];
			System.out.println(p1.toString());
		    }
		    break;
	    case 3:
		//int r = 0;
		if(!terminar){
		Pareja pareja = null;
		for(int i=0; i < parejas.length; i++){
		    pareja = (Pareja) parejas[i];
		    dia = dia1.agrupar(pareja);
		    Jugador j1 = (Jugador) pareja.getJugador(0);
		    Jugador j2 = (Jugador) pareja.getJugador(1);
		    if(j2.getNombre().equals("Ordenador")){
			dia = dia1.agrupar(j1);
		    } else{
			dia = dia1.agrupar(j1);
			dia = dia1.agrupar(j2);
		    }
		}
		System.out.println("\n      *** Ha decidido terminar esta etapa. ***");
		terminar = true;
		dia = dia1.agrupar((Boolean) terminar);
		dia1.escribirObjetos("./Archivos/Dia1.txt", dia);
		} else{
		    System.out.println("Adios ...");
		}
		break;
	    default:
		System.out.println("\n"+"Up's, algo salió mal...\n");
		break;
	    }
	} while(opcion != 3);
	System.out.println("\n"+"------------------------------------------------------\n");
    }
}
