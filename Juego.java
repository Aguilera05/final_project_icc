/**
 * <p>Interfaz para definir el comportamiento de objetos
 * que se puedan escalar.</p>
 * author Adrián Aguilera Moreno.
 * <p> Creador el 01/02/2021 </p>
 * @version 1.0
 * @see <a href="./Documents/Jugador.html">Jugador</a>
 */

public interface Juego{
    
    /**
     * <p>Método para saber quien es el ganador
     * por partida en un día de torneo.</p>
     * @param g1      -- Objeto <code>Jugador</code>.
     * @param g2      -- Objeto <code>Jugador</code>.
     * @return String -- El ganador en un torneo.
     */
    public String ganador(Jugador g1, Jugador g2);
    
    /**
     * <p>Método que detiene el juego cuando este llega
     * a su fin.</p>
     * @return boolean -- <code>True</code> o 
     *                    <code>False</code>.
     */
    public boolean fin();
}
