import java.util.Random;
/**
 * <p>Clase que implementa la clase <code>Juego</code>
 * y que crea un <code>MiniBlackJack</code>.</p>
 * @author Adrián Aguilera Moreno.
 * <p> Creada el 01/02/2021 </p>
 * @version 1.0
 * @see <a href="./Documents/Jugador.html">Jugador</a>
 * @see <a href="./Documents/Juego.html">Jugador</a>
 * @see <a href="./Documents/Carta.html">Carta</a>
 */

public class MiniBlackJack implements Juego{
    
    /* Atributos */
    private Carta[] cartas;
    private static int primero = 0;
    private static boolean quedarse1 = false;
    private static boolean quedarse2 = false;
    private static boolean hayGanador = false;
    
    /**
     * <p>Método constructor por omisión que construye
     * un juego de tipo <code>MiniBlackJack</code>.</p>
     */
    public MiniBlackJack(){
	this.cartas = new Carta[14];
	for(int i=0; i<(cartas.length -3); i++){
	    cartas[i] = new Carta(i +1);
	}
	cartas[11] = new Carta("J");
	cartas[12] = new Carta("K");
	cartas[13] = new Carta("Q");
    }
    
    /**
     * <p>Método que obtiene el arreglo de
     * <code>Cartas</code> durante el juego.</p>
     * @return Carta -- Arreglo de cartas. 
     */
    public Carta[] getCartas(){
	return this.cartas;
    }
    
    /*
     * <p>Método que devuelve un número aleatorio entre 
     * el número que se le pase cómo parámetro y 0.</p>
     * @param max  -- Número entre el cual se da el aleatorio.
     * @return int -- Valor aleatorio.
     */
    private int random(int max){
	return (int) Math.round(Math.random() * max + 0.5);
    }
    
    /**
     * <p>Método que revuelve las <code>Cartas</code>.</p> 
     */
    public void revolverCartas(){
	for(int i=0; i<cartas.length; i++){
	    int numero     = random(cartas.length -1);
	    Carta temporal = cartas[numero];
	    cartas[numero] = cartas[i];
	    cartas[i]      = temporal;
	}
    }
    
    /**
     * <p>Método que obtiene la <code>Carta</code>
     * más próxima de mano de barajas.</p>
     * @return Carta -- La primer carta de la mano
     *                 de barajas.
     */
    public Carta sacarCarta(){
	Carta primera = cartas[primero++];
	return primera;
    }
    
    // Este método debe ser boolean
    /**
     * <p>Método que regresa <code>False</code>
     * o <code>True</code> si un jugador decide
     * quedarse.</p>
     * @param a -- Objeto de la clase <code>Jugador</code>.
     */
    public void quedar(Jugador a){
	if(a.getNumero() == 1)
	    quedarse1 = true;
	else if(a.getNumero() == 2)
	    quedarse2 = true;
	else{
	    quedarse1 = false;
	    quedarse2 = false;
	}
    }
    
    /**
     * <p>Método que obtiene el ganador de la ronda.</p>
     * @param j1     -- Objeto de la clase <code>Jugador</code>.
     * @param j2     -- Objeto de la clase <code>Jugador</code>.
     * return String -- El ganador de la ronda.
     */
    @Override public String ganador(Jugador j1, Jugador j2){
	if(j1.getPuntaje() > j2.getPuntaje()){
	    this.hayGanador = true;
	    j1.morePuntaje();
	    //j2.lessPuntaje();
	    return "El ganador es: "+ j1.getNombre();
	}
	else if(j1.getPuntaje() < j2.getPuntaje()){
	    this.hayGanador = true;
	    //j1.lessPuntaje();
	    j2.morePuntaje();
	    return "El ganador es: "+ j2.getNombre();
	} else
	    return null;
    }
    
    /**
     * <p>Método que marca el fin del <code>Juego</code>.</p>
     * @return boolean -- <code>True</code> o <code>False</code>.
     */
     @Override public boolean fin(){
	boolean a = quedarse1 && quedarse2;
        if(hayGanador || a)
	    return true;
	else
	    return false;
    }
}
