/**
 * <p>Clase que simula el comportamiento de un
 * <code>Dado</code>.</p>
 * @author Adrián Aguilera Moreno.
 * <p> Creado el 13/02/2021 </p>
 * @version 1.0
 */

public class Dado{
    
    /* Atributos */
    private int cara1;
    private int cara2;
    private int cara3;
    private int cara4;
    private int cara5;
    private int cara6;
    
    /**
     * <p>Método constructor que crea un <code>Dado</code>
     * y le asigna valor a sus caras.</p>
     */
    public Dado(){
	this.cara1 = 1;
	this.cara2 = 2;
	this.cara3 = 3;
	this.cara4 = 4;
	this.cara5 = 5;
	this.cara6 = 6;
    }
    
    /**
     * Método que regresa el valor de la cara en cuestión.
     * @param a -- int: valor de la cara, debe estar 
     * entre 1 y 6.
     * @return int -- Cara en el <code>Dado</code>.
     */
    public int getCara(int a){
	if(a == 1)
	    return this.cara1;
	else if(a == 2)
	    return this.cara2;
	else if(a == 3)
	    return this.cara3;
	else if(a == 4)
	    return this.cara4;
	else if(a == 5)
	    return this.cara5;
	else if(a == 6)
	    return this.cara6;
	else
	    return 0;
    }
    
    /*
     * <p>Método que devuelve un número aleatorio entre 
     * el número que se le pase cómo parámetro y 0.</p>
     * @param max  -- Número entre el cual se da el aleatorio.
     * @return int -- Valor aleatorio.
     */
    private int random(int max){
	return (int) Math.round(Math.random() * max + 0.5);
    }
    
    /**
     * Método que devuelve un número para saber que
     * cara del dado se tira.
     * @return int -- Valor aleatorio.
     */
    public int tiraUno(){
	return random(6);
    }
}
