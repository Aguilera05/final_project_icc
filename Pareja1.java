import java.io.Serializable;
/**
 * Clase que extiende a la clase <code>Pareja</code> y 
 * especifica una pareja para poderla trabajar en el 
 * <code>Dia1</code>.
 * <p> Creado el 10/02/2021 </p>
 * @see <a href="./Documents/Pareja.html">Pareja</a>
 * @see <a href="./Documents/Jugador.html">Jugador</a>
 * @see <a href="./Documents/Complemento1.html">Complemento1</a>
 */

public class Pareja1 extends Pareja implements Serializable{
    
    /**
     * Método constructor por parámetros que
     * crea una pareja para el <code>Dia1</code>.
     * @param a -- <code>Jugador</code>.
     * @param b -- <code>Jugador</code>.
     */
    public Pareja1(Jugador a, Jugador b){
	super(a, b);
    }
    
    /**
     * Constructor por parámetro que construye una pareja1 con
     * un sólo jugador para que este juegue con el computador.
     * @param a -- <code>Jugador</code> para jugar con el ordenador.
     */
    public Pareja1(Jugador a){
	super(a);
    }
    
    // Método que llama una clase heredada de Jugador.
    /**
     * <p>Método que le asigna un número al jugador
     * para poder identificarlo.</p>
     * @param a -- Objeto de tipo <code>Complemento1</code>.
     * @param b -- Objeto de tipo <code>Complemento1</code>.
     */
    public void contar(Jugador a, Jugador b){
	a.cambioNumero(1);
	b.cambioNumero(2);
    }
}
