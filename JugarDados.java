/**
 * <p>Clase que implementa la clase <code>Juego</code>
 * y que crea un Juego con <code>Dados</code>.</p>
 * @author Adrián Aguilera Moreno.
 * <p> Creada el 13/02/2021 </p>
 * @version 1.0
 * @see <a href="./Documents/Jugador.html">Jugador</a>
 * @see <a href="./Documents/Juego.html">Juego</a>
 * @see <a href="./Documents/Dado.html">Dado</a>
 */

public class JugarDados implements Juego{
    
    /* Atributos */
    Dado dado1;
    Dado dado2;
    boolean hayGanador = false;
    
    /**
     * <p>Método constructor que crea el juego de
     * los dados.</p> 
     */
    public JugarDados(){
	this.dado1 = new Dado();
	this.dado2 = new Dado();
    }
    
    /**
     * Método devuelve un <code>Dado</code> en
     * caso de requerirlo.
     * @param a -- Dado: valor para elegir el dado.
     * @return Dado -- <code>Dado</code> en cuestión.
     */
    public Dado getDado(int a){
	if(a == 0)
	    return this.dado1;
	else if(a == 1)
	    return this.dado2;
	else
	    return null;
    }
    
    /**
     * <p>Método que devuelve una coordenada
     * con los valores del tiro de dados al
     * instante.</p>
     * @return String -- coordenada con el tiro
     *                   de <code>Dados</code>.
     */
    public String tirarDados(){
	int tiro1
	    = this.dado1.tiraUno();
	int tiro2
	    = this.dado2.tiraUno();
	 String num1
	     = String.valueOf(tiro1);
	 String num2
	     = String.valueOf(tiro2);
	 return num1 +","+ num2;
    }
    
    /**
     * <p>Método que modifica los puntajes de los jugadores y devuelve
     * una cadena que indica quien ha sido el ganador entre 2 jugadores.</p>
     * @param  j1     -- <code>Jugador</code> en este <code>Juego</code>.
     * @param  j2     -- <code>Jugador</code> en este <code>Juego</code>.
     * @return String -- Mensaje que contiene al ganador de esta contienda.
     */
    @Override public String ganador(Jugador j1, Jugador j2){
	if((j1.getPuntaje() -1) == 0 && (j2.getPuntaje() -1) == 0){
	    if(j1.getPuntaje() > j2.getPuntaje()){
		this.hayGanador = true;
		j1.morePuntaje();
		return "El ganador es: "+ j1.getNombre();
	    } else if(j2.getPuntaje() > j1.getPuntaje()){
		this.hayGanador = true;
		j2.morePuntaje();
		return "El ganador es: "+ j2.getNombre();
	    } else{
		return null;	    
	    }
	} else if((j1.getPuntaje() -1) == 0 && j2.getPuntaje() == 1){
	    this.hayGanador = true;
	    j1.morePuntaje();
	    return "El ganador es: "+ j1.getNombre();
	} else if((j1.getPuntaje() -1) == 0 && j2.getPuntaje() == 0){
	    this.hayGanador = true;
	    j1.morePuntaje();
	    return "El ganador es: "+ j1.getNombre();
	} else if(j1.getPuntaje() == 1 && (j2.getPuntaje() -1) == 0){
	    this.hayGanador = true;
	    j2.morePuntaje();
	    return "El ganador es: "+ j2.getNombre();
	} else if((j1.getPuntaje() -1) == 0 && j2.getPuntaje() == 0){
	    this.hayGanador = true;
	    j2.morePuntaje();
	    return "El ganador es: "+ j2.getNombre();
	} else if((j1.getPuntaje() -1) == 1 && (j2.getPuntaje() -1) == 1){
	    if(j1.getPuntaje() > j2.getPuntaje()){
		this.hayGanador = true;
		j1.morePuntaje();
		return "El ganador es: "+ j1.getNombre();
	    } else if(j2.getPuntaje() > j1.getPuntaje()){
		this.hayGanador = true;
		j2.morePuntaje();
		return "El ganador es: "+ j2.getNombre();
	    } else{
		return null;	    
	    }
	} else
	    return null;
    }
    
    /**
     * <p>Método que marca el final de este juego, por
     * pareja.</p>
     * @return boolean -- <code>True</code>o <code>False</code>
     *                    dependiendo el caso.
     */
    @Override public boolean fin(){
	if(hayGanador)
	    return true;
	else
	    return false;
    }
}
