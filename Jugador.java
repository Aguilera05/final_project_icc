import java.io.Serializable;
import java.util.Random;
/**
 * <p>Clase que crea un <code>Jugador</code> y simula 
 * el comportamiento de este.</p>
 * @author Adrián Aguilera Moreno.
 * <p>Creado el 27/01/2021</p>
 * @version 1.8
 */

public class Jugador implements Serializable{
    
    /* Atributos */
    boolean d2, d3;
    private int puntaje;
    private String sexo;
    private String clave;
    private String nombre;
    private String carrera;
    private int numeroJugador;
    private String fechaNacimiento;
    
    // private int puntajeDia1;
    // private Jugador jugadorDia1;
    
    /**
     * <p>Constructor por parámetros, crea un <code>Jugador</code> 
     * con base a: </p><ul>
     *             <li>Sexo. </li>
     *             <li>Nombre. </li>
     *             <li>Carrera </li>
     *             <li>Clave de jugador. </li>
     *             <li>Fecha de nacimiento. </li>
     *             </ul>
     * @param nombre   -- Nombre del jugador.
     * @param fN       -- Fecha de nacimiento del jugador.
     * @param sexo     -- Sexo del jugador.
     * @param carrera  -- Carrera a la que pertenece
     *                    el <code>Jugador</code>.
     */
    public Jugador(String nombre, String fN,
		   String sexo, String carrera){
	this.d2 = false;
	this.d3 = false;
	this.puntaje = 0;
	this.sexo = sexo;
	this.nombre = nombre;
	this.carrera = carrera;
	this.clave = clave(fN);
	this.numeroJugador = 0;
	this.fechaNacimiento = fN;
    }
    
    /**
     * <p>Método modificador que asigna un sexo al
     * <code>Jugador</code>.</p>
     * @param sexo -- String: Sexo del jugador.
     */
    public void setSexo(String sexo){
	this.sexo = sexo;
    }
    
    /**
     * <p>Método inspector que devuelve el sexo del
     * <code>Jugador</code>.</p>
     * @return String -- sexo del jugador.
     */
    public String getSexo(){
	return this.sexo;
    }
    
    /**
     * <p>Método modificador que asigna un nombre
     * al <code>Jugador</code>.</p>
     * @param nombre -- Nombre del jugador.
     */
    public void setNombre(String nombre){
	this.nombre = nombre;
    }
    
    /**
     * <p>Método inspector que devuelve el nombre del
     * <code>Jugador</code>.</p>
     * @return String -- Nombre del jugador.
     */
    public String getNombre(){
	return this.nombre;
    }
    
    /**
     * <p>Método modificador que asigna una carrera
     * al <code>Jugador</code>.</p>
     * @param carrera -- Carrera a la cual pertenece
     *                   el jugador.
     */
    public void setCarrera(String carrera){
	this.carrera = carrera;
    }
    
    /**
     * <p>Método inspector que devuelve la carrera del
     * <code>Jugador</code>.</p>
     * @return String -- Carrera del jugador.
     */
    public String getCarrera(){
	return this.carrera;
    }
    
    /**
     * <p>Método inspector que obtiene el puntaje
     * de un <code>Jugador</code>.</p>
     * @return int -- Puntaje del jugador.
     */
    public int getPuntaje(){
	return this.puntaje;
    }
    
    /**
     * <p>Método modificador que devuelve la fecha de nacimiento
     *  del <code>Jugador</code>, con formato:</p>
     * <ul>
     * <li>día/mes/año</li>
     * </ul>
     * @param dia  -- Día de nacimiento.
     * @param mes  -- Mes de nacimiento.
     * @param anio -- Año de nacimiento.
     */
    public void setFechaNacimiento(String dia, String mes,
				   String anio){
	String d = dia.trim();
	String m = mes.trim();
	String a = anio.trim();
	String fecha = d +"/"+ m +"/"+ a;
	this.fechaNacimiento = fecha;
    }
    
    /**
     * <p>Método  inspector que devuelve la fecha de
     * nacimiento del <code>Jugador</code>.</p>
     * @return String -- Fecha de nacimiento del jugador.
     */
    public String getFechaNacimiento(){
	return this.fechaNacimiento;
    }
    
    /**
     * <p>Método inspector que devuelve la clave de un
     * <code>Jugador</code>.</p>
     * @return String -- Clave del jugador.
     */
    public String getClave(){
	return this.clave;
    }
     
    /*
     * Método que genera numeros aleatorios entre 0 y max.
     * @return int -- regresa un número aleatorio.
     */
    private int aleatorio0_9() {
	return (int) Math.round(Math.random() * 9 + 0.5);
    }

    /*
     * Método que genera una clave para un jugador 
     * con 10 dígitos.
     * @param f       -- String: Fecha de nacimiento.
     * @return String -- Clave de un jugador.
     */
    private String clave(String f){ //  12/Enero/2934
	String claveJ = subsAnio(f) 
	    + aleatorio0_9() + subsMes(f)
	    + aleatorio0_9() + subsDia(f);
	return claveJ;
    }
    
    /*
     * Método que substrae el año de nacimiento del
     * jugador.
     * @param f       -- String: Fecha de nacimiento.
     * @return String -- indAnio.
     */
    private String subsAnio(String f){
	String indAnio = f.substring(6);
	return indAnio;
    }
    
    /*
     * Método que substrae el mes de nacimiento del
     * jugador.
     * @param f       -- String: Fecha de nacimiento.
     * @return String -- indMes.
     */
    private String subsMes(String f){
	String indMes = f.substring(3, 5);
	return indMes;
    }
    
    /*
     * Método que substrae el día de nacimiento del
     * jugador.
     * @param f       -- String: Fecha de nacimiento.
     * @return String -- indDia.
     */
    private String subsDia(String f){
	String indDia = f.substring(0, 2);
	return indDia;
    }

    /**
     * <p>Método que aumenta el puntaje de un
     * <code>Jugador</code> cuando este gana
     * una partida.</p>
     */
    public void morePuntaje(){
	this.puntaje += 1;
    }

    /**
     * <p>Método que disminuye el puntaje de un
     * <code>Jugador</code> cuando este pierde
     * una partida.</p>
     */
    public void lessPuntaje(){
	this.puntaje -= 1;
    }

    //--------------------------------------------------------------------
    /**
     * <p>Método que asigna un número al
     * <code>Jugador</code>.</p>
     * @param numero -- int: Valor a sustituir.
     */
    public void cambioNumero(int numero){
	this.numeroJugador = numero;
    }
    
    /**
     * <p>Método inspector que obtiene el número
     * de un jugador</p>
     * @return int -- Número del <code>Jugador</code>.
     */
    public int getNumero(){
	return this.numeroJugador;
    }
    
    // Tentativo a eliminar.
    /**
     * <p>Método que reinicia el número del jugador,
     * i.e. lo regresa a 0.</p>
     */
    public void reiniciarNumero(){
	this.numeroJugador = 0;
    }
    //--------------------------------------------------------------------
    
    /**
     * Método que registra <code>True</code>
     * en caso de haber sido aplicado.
     */
    public void setDia2(){
	this.d2 = true;
    }
    
    /**
     * Método que devuelve el valor del atributo marcado
     * para saber si ya jugo el día 2 o no.
     * @return boolean -- d2.
     */
    public boolean getDia2(){
	return this.d2;
    }
    
    /**
     * Método que registra <code>True</code>
     * en caso de haber sido aplicado.
     */
    public void setDia3(){
        this.d3 = true;
    }
    
    /**
     * Método que devuelve el valor del atributo marcado
     * para saber si ya jugo el día 2 o no.
     * @return boolean -- d3.
     */
    public boolean getDia3(){
	return this.d3;
    }
    
    /**
     * <p>Método que sobrescribe el método toString de
     * la clase java.util y devuelve los atributos de 
     * un <code>Jugador</code> en forma de un String.</p>
     * @return String -- Jugador en forma de String.
     */
    @Override public String toString(){
	return "Nombre: "+ this.nombre +".\n"
	    +"Fecha de nacimiento: "+
	    this.fechaNacimiento +".\n"
	    +"Carrera: "+ this.carrera +".\n"
	    +"Clave: "+ this.clave +".\n"
	    +"Sexo: "+ this.sexo +".\n";
    }
    
    /**
     * <p>Método que devuelve <code>True</code> o 
     * <code>False</code> si un <code>Jugador</code>
     * es igual a otro o no respectivamente.</p>
     * @param aComparar -- Objeto de tipo 
     *        <code>Jugador</code> que se compara
     *        con el <code>Jugador</code> que instancía
     *        al método.
     * @return boolean -- <code>True</code> o 
     *                    <code>False</code>.
     */
    public boolean equals(Jugador aComparar){
	if(this.clave.equals(aComparar.clave))
	    return true;
	else
	    return false;
    }
}
