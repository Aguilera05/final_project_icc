/**
 * <p>Clase para crear una excepcón que extiende de Excepcion</p>
 * @author Adrian Aguilera Moreno.
 * <p> Creado el 01/02/2021 </p>
 * @version 1.0
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html"></a>
 */

public class Excepciones extends Exception{
    
    /**
     * <p>Constructor por omisión que instancía
     * al constructor de la clase padre.</p>
     */
    public Excepciones(){
	super();
    }
    
    /**
     * <p>Constructor por parámetros que instancía
     * el constructor de la clase padre.</p>
     * @param msj -- String: Mensaje para la excepción.
     */
    public Excepciones(String msj){
	super(msj);
    }
    
}
