import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Random;
/**
 * <p>Clase que mantiene el control del 
 * menú durante el juego.</p>
 * @author Adrian Aguilera Moreno.
 * <p>Created on 29/01/2021</p>
 * @version 1.1
 */

public class Menu{
    
    public static void main(String [] pps){
	
	
	int opcion = 0;
	boolean terminar = false;
	Scanner in = new Scanner(System.in);
	Scanner on = new Scanner(System.in);
	Object[] jugadores = new Object[0];
	Object[] parejasD1 = new Object[0];
	Object[] parejasD2 = new Object[0];
	Object[] parejasD3 = new Object[0];
	Utilidades util = new Utilidades();
	jugadores = util.leerObjetos("./Archivos/Registros.txt");
	parejasD1 = util.leerObjetos("./Archivos/Dia1.txt");
	parejasD2 = util.leerObjetos("./Archivos/Dia2.txt");
	parejasD3 = util.leerObjetos("./Archivos/Dia3.txt");
	
	System.out.println("\n"+"------------------------------------------------------");
	System.out.println("        Bienvenido al Menú de esta temporada.");
	do{
	    System.out.println("\nA continuación se presentan las opciones:");
	    System.out.println("1. Concursantes registrados.");
	    System.out.println("2. Lista del torneo.");
	    System.out.println("3. Campeones.");
	    System.out.println("4. Salir.");
	    do{
		System.out.print("\n Elige una opción: ");
		try{
		    opcion = in.nextInt();
		    in.nextLine();
		} catch(InputMismatchException e){
		    System.out.println();
		    System.out.println("\nDebes introducir un número entre 1 y 4.\n");
		    in.nextLine();
		}
	    } while(opcion < 1 || opcion > 4);
	    
	    switch(opcion){
	    case 1:
		System.out.println("\nNúmero de jugadores: "+ (jugadores.length -1));
		Jugador cambio;
		for(int i=0; i < jugadores.length; i++){
		    if(jugadores[i] instanceof Jugador){
			cambio = (Jugador) jugadores[i];
			System.out.println(cambio.toString());
		    }
		}
		break;
	    case 2:
		if(parejasD1.length != 0){
		    System.out.println("\nParejas en el día 1:\n");
		    Pareja cambiar;
		    for(int i=0; i < parejasD1.length; i++){
			if(parejasD1[i] instanceof Pareja){
			    cambiar = (Pareja) parejasD1[i];
			    System.out.println(cambiar.toString());
			}
		    }
		}
		
		if(parejasD2.length != 0){
		    System.out.println("\nParejas en el día 2:\n");
		    Pareja cambiar;
		    for(int i=0; i < parejasD2.length; i++){
			if(parejasD2[i] instanceof Pareja){
			    cambiar = (Pareja) parejasD2[i];
			    System.out.println(cambiar.toString());
			}
		    }
		}
		
		if(parejasD3.length != 0){
		    System.out.println("\nParejas en el día 3:\n");
		    Pareja cambiar;
		    for(int i=0; i < parejasD3.length; i++){
			if(parejasD3[i] instanceof Pareja){
			    cambiar = (Pareja) parejasD3[i];
			    System.out.println(cambiar.toString());
			}
		    }
		}
		
		if((parejasD1.length == 0) && (parejasD2.length == 0) &&
		   (parejasD3.length == 0)){
		    System.out.println("\n***Aún no hay parejas.***\n");
		}
		break;
	    case 3:
		// Código para obtener el ganador por parejas.
		break;
	    case 4:
		System.out.println("Hasta luego ...");
		break;
	    default:
		System.out.println("Up's algo salió mal ...");
		break;
	    }
	}while(opcion != 4); //Cierre del primer do	    
    }
}
