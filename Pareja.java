import java.io.Serializable;
/**
 * <p>Clase que crea una <code>Pareja</code> mediante
 * dos jugadores para enfrentarlos durante el torneo.</p>
 * @author Adrián Aguilera Moreno.
 * <p>Creado el 01/02/2021</p>
 * @see <a href="./Documents/Jugador.html">Jugador</a>
 */
//meter la clase complemento como see
public class Pareja implements Serializable{

    /* Atributos */
    private Jugador jugador1;
    private Jugador jugador2;
    
    /**
     * <p>Método constructor por parámetros que crea
     * un <code>Jugador</code>.</p>
     * @param a -- <code>Jugador</code>.
     * @param b -- <code>Jugador</code>.
     */
    public Pareja(Jugador a, Jugador b){
	this.jugador1 = a;
	this.jugador2 = b;
	// System.out.println("Debe introducir un jugador");
	// e.printStackTrace();
    }
    
    /**
     * Constructor por parámetro que construye una pareja con
     * un sólo jugador para que este juegue con el computador.
     * @param a -- <code>Jugador</code> para jugar con el ordenador.
     */
    public Pareja(Jugador a){
	this.jugador1 = a;
	Jugador b
	    = new Jugador("Ordenador", "00/00/0000",
			  "Indefinido", "Computador");
	this.jugador2 = b;
    }
    
    /**
     * <p>Método inspector que devuelve un
     * <code>Jugador</code>.</p>
     * @param num      -- Número de <code>Jugador</code>, 0 o 1 
     *                    dependiendo del <code>Jugador</code>.
     * @return Jugador -- Objeto de la clase <code>Jugador</code>.
     */
    public Jugador getJugador(int num){
	if(num == 0)
	    return this.jugador1;
	else if(num == 1)
	    return this.jugador2;
	else{
	    System.out.println("Debes Introducir un número entre 1 y 2");
	    return null;
	}
    }
    
    /**
     * <p>Método modificador que asigna un nuevo jugador 
     * para una pareja de jugadores</p>
     * @param num -- Número de <code>Jugador</code>, 0 o 1 
     *               dependiendo del <code>Jugador</code>.
     * @param a   -- Objeto de la clase <code>Jugador</code>.
     */
    public void setJugador(int num, Jugador a){
	if(num == 0)
	    this.jugador1 = a;
	else if(num == 1)
	    this.jugador2 = a;
	else
	    System.out.println("Introduzca un valor entre 1 y 2.");
	//Crear Excepción "Parámetro inválido..."
	//System.out.println("Debe introducir un jugador...");
	//e.printStackTrace();
	
    }
    
    /**
     * <p>Método que sobrescribe el método toString de
     * la clase java.util y devuelve los nombres y claves
     * de los jugadores en forma de un String.</p>
     * @return String -- Jugador en forma de String.
     */
    @Override public String toString(){
	String a = this.jugador1.getNombre();
	String b = this.jugador2.getNombre();
	String c1 = this.jugador1.getClave();
	String c2 = this.jugador2.getClave();
	String d = "Se enfrentan: \n";
	String l = "---------------------------";
	return d + l + l +"\n Jugador 1: "+ a
	    +"\n Clave: "+ c1 +"\n"
	    +"\n Jugador 2: "+ b + "\n Clave: "+ c2
	    +"\n"+ l + l + "\n";
    }

    /**
     * <p>Método que devuelve <code>True</code> o 
     * <code>False</code> si una <code>Pareja</code>
     * es igual a otra o no respectivamente.</p>
     * @param pareja -- Objeto de tipo <code>Pareja</code> que 
     *                  se compara con la <code>Pareja</code> 
     *                  que instancía al método.
     * @return boolean -- <code>True</code> o 
     *                    <code>False</code>.
     */
    public boolean equals(Pareja pareja){
	Jugador a1 = pareja.getJugador(0);
	Jugador b1 = pareja.getJugador(1);
	Jugador a2 = this.getJugador(0);
	Jugador b2 = this.getJugador(1);
	if(a1.equals(a2) && b1.equals(b2))
	    return true;
	else
	    return false;
	//Crear una excepción para parejas.
	// System.out.println("Debe introducir una pareja...");
	// e.printStackTrace();	
    }
}
